# README #

#Pre-Requisitos#

Este modulo é um sub-modulo do WebAPI para o SNEP.

Depende:
- Módulo WebAPI

#Instalação#

Descompactar módulo na pasta snep/modules/webapi/api/services/

```
#!shell

tar xzf webapi-manager.tar.gz -C snep/modules/webapi/api/services/

```


#Uso#
