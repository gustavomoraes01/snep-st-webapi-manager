<?php
/* 
    Class to manage User for WebApi
    Used to implement some functions to be used in the requests of WebApi.

Author: Flavio Somensi
*/

class apiUser {


    /**
    * Verify parameters 
    * @param <array> $param
    */

    public function verifyParameters($param,$action) {

        // Verify ClientId / UserId for add and addComplete
        if ( $action === "addComplete" || $action === "add" ) {
            if ( !isset($param->user_id) || !isset($param->client_id) || self::verifyUser($param->client_id, $param->user_id) ) {
		    api_error("apiUser: The parameter 'user_id='".$param->user_id." was not informed or already exists a user for the client informed. Verify.");
            }
        }

        if ($action != "addComplete" ) {
            if ( $action === "update" || $action === "delete" ) {
                if (! self::verifyUser($param->client_id, $param->user_id) ) {
                    api_error("apiUser: No registers found for parameters client_id and user_id. Verify.");
                }
            } else {
                // Verify Client Id
                if (! self::verifyClientId($param->client_id) ) {
                    api_error("apiUser: The parameter 'client_id' was not informed or is incorrect. Check the documentation.");
                }
            }
            // Verify client_secret 
            if (!isset($param->client_secret) && ($action === "add" ) ) {
                api_error("apiUser: The parameter 'client_secret' was not informed. Check the documentation.");
            } elseif (isset($param->client_secret) && $param->client_secret === "" ) {
                api_error("Client: The parameter 'client_secret' can not be NULL. Check the documentation.");
            } 

            // Verify scope - OPTIONAL 
            if (!isset($param->scope)) {
                $param->scope = $this->config->user->scope ;
            } 
                
        }
        return true;
    }

    /**
    * Add new API User
    * @param <array> $data - UserAPI data
    * @param <string> $action - Actions : add / addComplete
    * @param <object> $mdb - Connection with database
    */
    public function add($data,$action,$mdb = NULL) {

        if ($action === "addComplete") {

            self::verifyParameters($data,$action);

            $data->client_secret = self::makeSecret() ;

        }

        $insert_data = array(
            "client_id" => $data->client_id,
            "client_secret" => $data->client_secret,
            "redirect_uri" => '', 
            "grant_types" => '',
            "scope" => $data->scope,
            "user_id" => $data->user_id,

        );

        if ($mdb === NULL) {
            $mdb = Zend_Registry::get('mdb');
        }

        $mdb->insert('oauth_clients', $insert_data);

        if ($action === "addComplete") {
            return $data->client_secret ;
        } else {
            api_return(array( "status" => "ok" ));
        }
    }

   /**
    * Update User
    * @param <array> $data - apiUser data
    * @return <status> 
    */
    public function update($data) {

        $client_id = $data->client_id ;
        $user_id = $data->user_id ;

        $mdb = Zend_Registry::get('mdb');

        $select = "select * from oauth_clients where client_id = '".$client_id."'";
        $select .= " AND user_id = '".$user_id."'" ;

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        $update_data = array(
            "client_secret" => isset($data->client_secret) ? $data->client_secret : $result['client_secret'],
            "redirect_uri" => isset($data->redirect_uri) ? $data->redirect_uri : $result['redirect_uri'], 
            "grant_types" => isset($data->grant_types) ? $data->grant_types : $result['grant_types'],
            "scope" => isset($data->scope) ? $data->scope : $result['data_scope']
        );
        $where[] = "client_id = '{$client_id}' " ;
        $where[] = "user_id = '${user_id}'" ;

        $mdb->beginTransaction() ;

        try {
            
            $mdb->update('oauth_clients', $update_data, $where );

        $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "ok" ));
       
    }

    /**
    * Delete apiUser and your tokens
    * @param <array> $data - apiUser data
    * @param <object> $mdb - Database connection 
    * @return <status> 
    */
    public function delete($data, $mdb=NULL) {

        $where = array('client_id = ? ' => $data->client_id ) ;

        // Tokens
        include_once("../tokenUser.php") ;
        $delete_token = (object) array( 
                "client_id" => $data->client_id
        );

        if ($mdb === "NULL") {
            
            $mdb = Zend_Registry::get('mdb');
            $mdb->beginTransaction() ;
            $flag = true;
        }

        try {

            tokenUser::delete($delete_token,$mdb) ;
            
            $mdb->delete('oauth_clients', $where );

            if ( $flag ) {
                $mdb->commit() ;
            }

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        if ( $flag ) {
            api_return(array( "status" => "deleted"));
        }
    }

    /**
    * Verify Client_id
    * @param <string> $client_id
    * @return <boolean>
    */
    public function verifyClientId($client_id) {

        $mdb = Zend_Registry::get('mdb');

        $select = "select cod_client from automation_client where cod_client =  '" . $client_id . "'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return is_array($result) ? true : false ;

    }

    /**
    * Verify if exist a user for the client
    * @param <string> $client_id
    * @return <boolean>
    */
    public function verifyUserId($client_id) {

        $mdb = Zend_Registry::get('mdb');

        $select = "select user_id from oauth_clients where client_id =  '" . $client_id . "'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return is_array($result) ? false : true ;
    }

    /**
    * Verify if exist API User 
    * @param <string> $client_id
    * @param <string> $user_id
    * @return <boolean>
    */
    public function verifyUser($client_id, $user_id) {

        $mdb = Zend_Registry::get('mdb');

        $select = "select user_id from oauth_clients where client_id =  '" . $client_id . "'";
        $select .= " AND user_id = '" . $user_id."' " ;

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return is_array($result) ? true : false ;

    }
    /**
    * Make a new client_secret
    * @return <string> $client_secret
    */
    public function makeSecret() {

        $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%*-' ;
        $tamanho = 6 ;

        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }
        return $retorno;

    }


}
