<?php
/* 
    Class to Manager Client .
    Used to implement some functions to be used in the requests of Manager Client.

    Author: Flavio H Somensi
*/

class client {

    /**
    * Verify parameters 
    * @param <array> $param
    */

    public function verifyParameters($param, $action) {

        if ( $action === "update" || $action === "delete" ) {
            include_once("../apiUser.php") ;
            if (! apiUser::verifyClientId($param->client_id) ) {
                api_error("client: No registers found for parameters client_id. Verify.");
            }
        }

        // Verify cnpj
        if (!isset($param->cnpj) && ($action === "add" || $action === "addComplete"))  {
            api_error("client: The parameter 'cnpj' was not informed. Check the documentation.");
        } elseif (isset($param->cnpj) && strlen($param->cnpj) > 14 ) {
            api_error("client: The parameter 'cnpj' is incorrect - 14 numbers required. Check the documentation.");
        }

        // Verify email
        if ((!isset($param->email) || $param->email === "") && ($action === "add" || $action === "addComplete") ) {
            api_error("client: The parameter 'email' was not informed or is incorrect. Check the documentation.");
        }

        // Verify if E-mail and cnpj are not duplicated
        if (self::is_duplicated($param->cnpj,$param->email)) {
            api_error("client: The parameter 'email' our 'cnpj' already exists. Verify.");
        }

        // Verify name
        if (!isset($param->name) && ($action === "add" || $action === "addComplete"))  {
            api_error("client: The parameter 'name' was not informed. Check the documentation.");
        } elseif (isset($param->name) && strlen($param->name) < 2 ) {
            api_error("client: The parameter 'name' is incorrect. Min 3 characters. Check the documentation.");
        }

        // Verify ddd
        if (!isset($param->ddd) && ($action === "add" || $action === "addComplete"))  {
            api_error("Client: The parameter 'ddd' was not informed. Check the documentation.");
        } elseif ( isset($param->ddd) && (strlen($param->ddd) > 3 || !is_numeric($param->ddd))) {
            api_error("client: The parameter 'ddd' is incorrect. Check the documentation.");
        }

        // Verify did
        if (!isset($param->did) && ($action === "add" || $action === "addComplete") ) {
            api_error("Client: The parameter 'did' was not informed. Check the documentation.");
        } elseif ( isset($param->did) && (strlen($param->did) > 12 || !is_numeric($param->did))) {
            api_error("client: The parameter 'did' is incorrect. Check the documentation.");
        }

        // Verify email
        if (!isset($param->email)  && ($action === "add" || $action === "addComplete") ) {
            api_error("client: The parameter 'email' was not informed or is incorrect. Check the documentation.");
        }

        // OPTIONAL Values -  for Default, verify manager/confs/setup.conf
        // Verify value_alert - OPTIONAL
        if (isset($param->value_alert) ) {
            if ($param->value_alert < 1 || !is_numeric($param->value_alert)) {
                api_error("client: The parameter 'value_alert' was not informed or is incorrect. Check the documentation.");
            }
        } else {
            $param->value_alert = $this->config->client->value_alert ;
        }
        // Verify factor - OPTIONAL 
        if (isset($param->factor)) {
            if ( $param->factor < 0) {
                api_error("client: The parameter 'factor' was not informed or is incorrect. Check the documentation.");
            }
        } else {
            $param->factor = $this->config->client->factor ;
        }
        // Verify amd - OPTIONAL 
        if (isset($param->amd)) {
            if ($param->amd != "yes" || $param->amd != "no") {
                api_error("client: The parameter 'amd' was not informed or is incorrect. Check the documentation.");
            }
        } else {
            $param->amd = $this->config->client->amd === '1' ? 'yes' : 'no' ;
        }
        // Verify timezone - OPTIONAL 
        if (!isset($param->timezone)) {
            $param->timezone = $this->config->client->timezone ;
        }

        
        return true;

    }

    /**
    * Method Get client by email
    * @param <array> $data
    * @return <array> $result
    */
    public function get($data) {

        $mdb = Zend_Registry::get('mdb');
        $mail = $data->parameters[0]->email;

        $select = "select * from automation_client where email like '%{$mail}%'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return api_return(array( "cod_client" => $result['cod_client'],
				 "name" => $result['name'],
				 "did" => $result['did'],
                 "email" => $result['email']));
    }

    /**
    * Add new Client
    * @param <array> $data - Client data
    * @return <int> $cod_client
    */
    public function add($data) {

        $data->cod_client = self::getnext() ;

        $insert_data = array(
            "cod_client" => $data->cod_client,
            "name" => $data->name,
            "ddd" => $data->ddd,
            "did" => $data->did,
            "factor" => $data->factor,
            "amd" => $data->amd,
            "email" => $data->email,
            "timezone" => $data->timezone,
            "value_alert" => $data->value_alert,
            "created" => date('Y-m-d H:i:s'),
            "updated" => date('Y-m-d H:i:s'));

        $mdb = Zend_Registry::get('mdb');

        $mdb->beginTransaction() ;

        try {

            $mdb->insert('automation_client', $insert_data);

            $now = date('Y-m-d H:i:s');
            $expire = date('Y-m-d', strtotime("+30 days"));
            $expire .= " 23:59:59";

            // Insert credits register
            $insert_credits = array(
                "cod_client" => $data->cod_client ,
                "value" => isset($data->credits_value) ? $data->credits_value : 25,
                "date_init" => $now,
                "date_expire" => isset($data->expire) ? $data->expire : $expire,
                "id_plan" => isset($data->id_plan) ? $data->id_plan : 4,
                "service_name" => isset($data->service) ? $data->service : "Voice Contact"
            ) ;

            $mdb->insert('automation_credits', $insert_credits);

            $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "client_id" => $data->cod_client ));

    }

    // control payment
    public function addPaymentControl($data){

        $insert_data = array(
            "cod_client" => $data->cod_client,
            "date" => date('Y-m-d H:i:s'),
            "service" => $data->service,
            "token"   => $data->token,
            "trial_utilized" => $data->trial,
            "token_enable" => false);

        $mdb = Zend_Registry::get('mdb');
        $mdb->beginTransaction() ;

        try{
            $mdb->insert('payment_control', $insert_data);
            $mdb->commit() ;
            api_return(array( "status" => "insert"));
        }catch (Exception $e){
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

    }

    public function getPaymentControl($data) {

        $mdb = Zend_Registry::get('mdb');

        $cod_client = $data->parameters[0]->cod_client;
        $service = $data->parameters[0]->service;
        $select = "select * from payment_control where cod_client = '".$cod_client."' AND service = '".$service."' AND token_enable = '0'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return api_return(array( "cod_client" => $result['cod_client'],
                 "service" => $result['service'],
                 "trial_utilized" => $result['trial_utilized'],
                 "token" => $result['token']));
    }

    public function updatePaymentControl($data) {

        $mdb = Zend_Registry::get('mdb');

        $client_id = $data->parameters[0]->cod_client;
        $service = $data->parameters[0]->service;

        $update_data = array("token_enable" => true);

        $mdb->beginTransaction() ;

        try {

            $mdb->update('payment_control', $update_data, "cod_client = '{$client_id}' AND service = '{$service}'");
            $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "ok" ));

    }

   /**
    * Add new Client, userApi, userToken and userService
    * @param <array> $data - data
    * @return <string> $client_secret - password generated
    */
    public function addComplete($data) {

        $data->cod_client = self::getnext() ;

        $insert_data = array(
            "cod_client" => $data->cod_client,
            "name" => $data->name,
            "ddd" => $data->ddd,
            "did" => $data->did,
            "factor" => $data->factor,
            "amd" => $data->amd,
            "email" => $data->email,
            "timezone" => $data->timezone,
            "cnpj" => $data->cnpj,
            "value_alert" => $data->value_alert,
            "created" => date('Y-m-d H:i:s'),
            "updated" => date('Y-m-d H:i:s'));

        $mdb = Zend_Registry::get('mdb');

        $mdb->beginTransaction() ;

        try {

            // 1 - Insert client 
            $mdb->insert('automation_client', $insert_data);

            $now = date('Y-m-d H:i:s');
            $expire = date('Y-m-d', strtotime("+30 days"));
            $expire .= " 23:59:59";

            // Insert credits register 
            $insert_credits = array( 
                "cod_client" => $data->cod_client ,
                "value" => isset($data->credits_value) ? $data->credits_value : 25,
                "date_init" => $now,
                "date_expire" => isset($data->expire) ? $data->expire : $expire,
                "id_plan" => isset($data->id_plan) ? $data->id_plan : 4, 
                "service_name" => isset($data->service) ? $data->service : "Voice Contact"
            ) ;

            $mdb->insert('automation_credits', $insert_credits);


            // 2 - Insert userApi
            include_once("../apiUser.php") ;
            $insert_user = (object) array( 
                "name" => $data->name, 
                "client_id" => $data->cod_client ,
                "user_id" => $data->user_id,
                "scope" => isset($data->scope) ? $data->scope : 'all'
            ) ;

            $client_secret = apiUser::add($insert_user,'addComplete',$mdb) ;

            // verify if trial plan
            $expire_condition = false;
            if(isset($data->expire)){
                $expire_condition = $data->expire;
            }
            // 3 - Insert userToken
            include_once("../tokenUser.php") ;
            $insert_token = (object) array( 
                "client_id" => $data->cod_client,
                "user_id" => $data->user_id,
                "scope" => isset($data->scope) ? $data->scope : 'all',
                "type" => 'permanent'
            );
            $token = tokenUser::add($expire_condition, $insert_token,'addComplete',$mdb) ;

            // 4 - Insert serviceUser
            if (isset($data->services)) { 
                include_once("../serviceUser.php") ;
                $insert_service = (object) array( 
                    "client_id" => $data->cod_client,
                    "services" => $data->services,
                );
                $service = serviceUser::add($insert_service,'addComplete',$mdb) ;
            }

            $mdb->commit() ;

            if (isset($service['connection'])) {
                $service['connection']->commit() ; 
            }
            

        } catch (Exception $e) {

            $mdb->rollBack();

            if (isset($service['connection'])) {
                $service['connection']->rollBack() ; 
            }

        
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        
        }

        $ret = array(
            "client_id" => $data->cod_client,
            "client_secret" => $client_secret,
            "access_token" => $token['access_token'],
            "expires" => $token['expires'],
        ) ;
        if (isset($service['ids']) ) {
            $ret["ids"] = $service['ids'] ;
        }
        api_return($ret) ;
    }

    /**
    * Update  Client
    * @param <array> $data - Client data   
    * @return <string> $status
    */
    public function update($data) {

        $mdb = Zend_Registry::get('mdb');

        $client_id = $data->client_id;

        $select = "select * from automation_client where cod_client =  '" . $client_id . "'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        $update_data = array(
            "name" => isset($data->name) ? $data->name : $result['name'],
            "ddd"  => isset($data->ddd)  ? $data->ddd : $result['ddd'],
            "did"  => isset($data->did)  ? $data->did : $result['did'],
            "factor" => isset($data->factor) ? $data->factor : $result['factor'],
            "amd"    => isset($data->amd) ? $data->amd : $result['amd'],
            "email"  => isset($data->email) ? $data->email : $result['email'],
            "timezone"    => isset($data->timezone) ? $data->timezone : $result['timezone'],
            "value_alert" => isset($data->value_alert) ? $data->value_alert : $result['value_alert'],
            "updated" => date('Y-m-d H:i:s'));

        $mdb->beginTransaction() ;

        try {
            
            $mdb->update('automation_client', $update_data, "cod_client = '{$client_id}' ");

            // Update credits register 
            if (isset($data->credits_value)) {
                $update_credits = array( 
                    "value" => $data->credits_value 
                ) ;
                $mdb->update('automation_credits', $update_credits,"cod_client = '{$client_id}' ");
            }

            $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "ok" ));
        
    }


    /**
    * Delete Client and User and Token and services
    * @param <array> $data - Client data   
    * @return <string> $status
    */
    public function delete($data) {

        $client_id = $data->client_id;

        $mdb = Zend_Registry::get('mdb');

        $mdb->beginTransaction() ;

        try {

            // services - automation
            include_once("services/automation/lib/Automation.php");
            $delete_service = (object) array( 
                "client_id" => $client_id 
            ) ;
            Automation::delete($delete_service,$mdb) ;

            // userApi - delete tokens also - cascade
            include_once("../apiUser.php") ;
            $delete_user = (object) array( 
                "client_id" => $client_id 
            ) ;
            apiUser::delete($delete_user,$mdb) ;           

            // Delete credits register 
            $mdb->delete('automation_credits',"cod_client = '{$client_id}' ");

            $mdb->delete("automation_client","cod_client = '{$client_id}'") ;

            $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "deleted" ));
    }

    /**
    * Method Get next client code 
    * @return <int> $result
    */
    public static function getnext() {

        $db = Zend_Registry::get('mdb');

        $select = "select max(cod_client) as cod_client from automation_client";

        $stmt = $db->query($select);
        $result = $stmt->fetch();

        if($result['cod_client'] == ""){
            $res = "1000";
        }else{
            $res = $result['cod_client'] + 1;
        }

        return (int) $res;
    }

    /**
    * Method to verify duplicated cnpj and/or email
    * @return <boolean> $result
    */
    public static function is_duplicated($cnpj,$email) {
        $db = Zend_Registry::get('mdb');

        $select = "select cnpj,email from automation_client";
        $select.= " where email = '".$email."'";
        //$select.= " where cnpj = '".$cnpj."' or email = '".$email."'";

        try {
            $stmt = $db->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return is_array($result) ? true : false ;

    }


}
