<?php
/* 
    Class to manage Client Credits for WebApi
    Used to implement some functions to be used in the requests of WebApi.

Author: Flavio Somensi
*/

class credits {

    /**
    * Verify parameters
    * @param <array>  
    */

    public function verifyParameters($param, $action) {

        if (!isset($param->client_id) ) {
            api_error("credits: Parameters client_id not informed. Verify.");
        }

        // Verify Value
        if ( !isset($param->credits_value)) {
            api_error("credits: The parameter 'credits_value' was not informed. Check the documentation.");
        }
        
        return true; 

    }

    /**
    * Update credit
    * @param <array> $data - credits data
    */
    public function update($data) {

        $client_id = $data->client_id ;
        $value = $data->value ;

        $mdb = Zend_Registry::get('mdb');

        $select = "select * from automation_credits where cod_client = '".$client_id."'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        if (!is_array($result)) {
            api_error("credits: Client not exists.");
        }

        $update_data = array(
            "value" => $data->value + $result['value'] 
        );
        $where[] = "cod_client = '{$client_id}' " ;


         $mdb->beginTransaction() ;

        try {
            
            $mdb->update('automation_credits', $update_data, $where );

        $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "ok" ));
    }


    /**
    * Add new Credits
    * @param <array> $data - Credits data   
    * @return <int> $cod_client
    */
    public function add($data) {

        $mdb = Zend_Registry::get('mdb');
        $mdb->beginTransaction() ;

        $now = date('Y-m-d H:i:s');
        $expire = date('Y-m-d', strtotime("+30 days"));
        $expire .= " 23:59:59";

        // Insert credits register 
        $insert_credits = array(
            "cod_client" => $data->cod_client ,
            "value" => isset($data->credits_value) ? $data->credits_value : 25,
            "date_init" => $now,
            "date_expire" => isset($data->expire) ? $data->expire : $expire,
            "id_plan" => isset($data->id_plan) ? $data->id_plan : 4,
            "service_name" => isset($data->service) ? $data->service : "Voice Contact"
         ) ;                

	try {

            $mdb->insert('automation_credits', $insert_credits);
            $mdb->commit() ;

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "ok" ));
    }
 

}
