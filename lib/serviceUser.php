<?php
/* 
    Class to manage Service User for WebApi
    Used to implement some functions to be used in the requests of WebApi.

Author: Flavio Somensi
*/

class serviceUser {


    /**
    * Verify parameters for serviceUser
    * @param <array>  $param - serviceUser data
    * @param <string> $action - Actions : add / addComplete
    */
    public function verifyParameters($param,$action) {

        // Verify if service exist and load servic file with methods
        foreach ($param->services as $key => $val) {
            foreach ($val as $key_name => $val_param) { 
                $file = "services/".strtolower($key_name)."/lib/".$key_name.".php";
                if (!file_exists($file)) {
                    api_error("serviceUser: The service name = '".$key_name."' not exist. Check the documentation.");
                } else {
                    require_once($file) ;
                }

            }
        }

        if ($action != "addComplete" ) {

            // Verify User Id
            include_once("../apiUser.php") ;
            if ( ! apiUser::verifyUser($param->client_id,$param->user_id)) {
                api_error("serviceUser: The parameter 'user_id' was not informed or is incorrect. Check the documentation.");
            }
           
            // Verify Client Id
            if (! apiUser::verifyClientId($param->client_id) ) {
                api_error("serviceUser: The parameter 'client_id' was not informed or is incorrect. Check the documentation.");
            }

            
        }

        return true;
    }

    /**
    * Add new ServiceUser
    * @param <array> $data - serviceUser data
    * @param <string> $action - Actions : add / addComplete
    * @param <object> $mdb - Database connection for action addComplete
    */
    public function add($data,$action,$mdb = NULL) {

        // A Fazer - Quardar registro em banco de dados de cada servico usado pelo client

         if ($action === "addComplete" ) {
            self::verifyParameters($data,$action);
        }
      
        
        foreach ($data->services as $key => $value) {

            foreach ($value as $service_key => $service_value) {

                $service_name = strtolower($service_key) ;

                $service_data = (object) array(
                    "user_id" => $data->user_id,
                    "client_id" => $data->client_id,
                    "parameters" =>  $service_value,
                    "type" => $data->type
                );

                switch ($service_name) {

                    case 'automation':
                                 
                        $result = Automation::add($service_data, $action, $mdb) ;
                        break;

                    case 'rd':
                        $result = rd::add($service_data, $action, $mdb) ;
                        break;

		   default: 
			api_error("Service ".$service_name." not found");
			break;
                }
            }

        }
        if ($action === "addComplete") {
            return $result ;
        } elseif ($action === "add") {
            api_return($result) ;
        }
    }




}
