<?php
/* 
    Class to manage Token User for WebApi
    Used to implement some functions to be used in the requests of WebApi.

Author: Flavio Somensi
*/

class tokenUser {

    /**
    * Verify parameters for tokenUser
    * @param <array>  
    */

    public function verifyParameters($param, $action) {

        if (!isset($param->client_id) || !isset($param->user_id)) {
            api_error("tokenUser: Parameters client_id and/or user_id not informed. Verify.");
        }

        if ($action === "delete") {

            $this->total = self::verifyUserTokens($param->client_id, $param->user_id) ;
            if ( $this->total === "0" ) {
                api_error("tokenUser: There are no records for the informed parameters. Verify.");
            }

        } else {

            // Verify Type
            if ( !isset($param->type) || ($param->type != "permanent" && $param->type != "temporary ")) {
                api_error("tokenUser: The parameter 'type' was not informed or is incorrect. Check the documentation.");
            }

            // Verify User Id
            if ( ! self::verifyUser($param->user_id)) {
                api_error("tokenUser: The parameter 'user_id' was not informed or is incorrect. Check the documentation.");
            }
            // Verify Client Id
            include_once("../apiUser.php") ;
            if (! apiUser::verifyClientId($param->client_id) ) {
                api_error("apiUser: The parameter 'client_id' was not informed or is incorrect. Check the documentation.");
            }
        }
        return true; 

    }

    /**
    * Add new tokenUser
    * @param <array> $data - tokenUser data
    * @param <string> $action - Actions : add / addComplete
    */
    public function add($expire_condition, $data,$action,$mdb = NULL) {

        $token = self::makeToken($data->client_id,$data->scope) ;

        if($expire_condition == 'trial'){
            $token['expires'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '+30 days'));
        }elseif($expire_condition == 'disabled'){
            $token['expires'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '-1 day'));
        }

        $insert_data = array(
            "client_id" => $data->client_id,
            "access_token" => $token['access_token'],
            "scope" => $data->scope,
            "user_id" => $data->user_id,
            "expires" => $token['expires'],
            "type" => $data->type
        );

        if ($mdb === NULL) {
            $mdb = Zend_Registry::get('mdb');
        }

        $mdb->insert('oauth_access_tokens', $insert_data);
        if ($action === "addComplete" ) {
            return $token ;
        } elseif ($action === "add") {
            api_return(array("access_token" => $insert_data['access_token'],
                             "expires" => $insert_data['expires'])) ;
        }
    }

    /**
    * Delete expired Tokens
    * @param <array> $data - tokenUser data
    * @param <object> $mdb - Database connection 
    * @param <status> 
    */
    public function delete($data,$mdb=NULL) {

        if ($mdb === "NULL") {
            
            $where = array('client_id = ? ' => $data->client_id ,
                'user_id = ? ' => $data->user_id,
                'type = ? ' => 'temporary' );

            $mdb = Zend_Registry::get('mdb');
            $mdb->beginTransaction() ;
            $flag = true ;

        } else { 

            if (isset($data->user_id)) {  // source in apiUser.php

                $where = array('client_id = ? ' => $data->client_id ,
                'user_id = ? ' => $data->user_id) ;

            } else {  // source in client.php

                $where = array('client_id = ? ' => $data->client_id ) ;

            }
        }

        try {
            
            $mdb->delete('oauth_access_tokens', $where );

            if ( $flag ) {
                $mdb->commit() ;
            }

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        if ($flag) {
            api_return(array( "status" => "deleted", "registers" => $this->total ));
        }
       
    }

    public function update($data){

        $mdb = Zend_Registry::get('mdb');

        $client_id = $data->cod_client;
        $services  = ",".$data->service;
        $expire    = $data->expire;

        try {

            $sql = "Update oauth_access_tokens SET expires = '{$expire}', scope=concat(scope, '{$services}') where client_id = '{$client_id}'";
            $mdb->query($sql);

        } catch (Exception $e) {

            $mdb->rollBack();
            api_error("An unexpected error has occurred: " . $e . " Contact system administrator.");
        }

        api_return(array( "status" => "ok" ));

    }

    /**
    * Verify if exist tokens for user and client
    * @param <string> $client_id
    * @param <string> $user_id
    * @return <boolean>
    */
    public function verifyUserTokens($client_id, $user_id) {

        $mdb = Zend_Registry::get('mdb');

        $select = $mdb->select()
                ->from('oauth_access_tokens',array('count(*) as total'))
                ->where('client_id = ? ',$client_id)
                ->where('user_id = ? ', $user_id)
                ->where('type = ?' , $temporary) ;

        $stmt = $mdb->query($select);
        $result = $stmt->fetchAll();

        return is_array($result) ? $result[0]['total'] : 0 ;

    }


    /**
    * Verify if exist a user
    * @param <string> $client_id
    * @return <boolean>
    */
    public function verifyUser($user_id) {

        $mdb = Zend_Registry::get('mdb');

        $select = "select user_id from oauth_clients where user_id =  '" . $user_id . "'";

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
        } catch (Exception $e) {
            return false ;
        }

        return is_array($result) ? true : false ;

    }


    /**
    * Make a new token
    * @param <string> $client_id
    * @param <string> $scope
    * @return <array> $result (token,expires)
    */
    public function makeToken($client_id, $scope) {

        $now = time();

        $access_token = md5($client_id."-".$scope."-".$now);
        $expires = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '+1 year'));

        return array("access_token" => $access_token, "expires" => $expires) ;

    }


}