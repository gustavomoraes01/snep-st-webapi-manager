<?php
/* 
    Class to manage valids scope for tokens
    Used to implement some functions to be used in the requests of WebApi.

Author: Flavio Somensi
*/

class validate {

    /**
    * Verify parameters
    * @param <array>  
    */

    public function verifyParameters($param, $action) {

        if (!isset($param->token) ) {
            api_error("validate: Parameter token not informed. Verify.");
        }
        
        return true; 

    }

    /**
    * get scope
    * @param <array> $data - validate data
    */
    public function get($data) {

        $token = isset($data->parameters[0]->token) ? $data->parameters[0]->token : $data->token ;

        $mdb = Zend_Registry::get('mdb');

        $select = "select expires,scope,type,client_id from oauth_access_tokens where access_token = '".$token."'";
        $select .= " and expires >= now() " ;

        try {
            $stmt = $mdb->prepare($select) ;
            $stmt->execute() ;
            $result = $stmt->fetch();
            $scope = explode(",", $result['scope']);
        } catch (Exception $e) {
            return false ;
        }

        if (!is_array($result)) {
            api_error("validate: Token not exists or is out of date.");
        };

        api_return(array( "scope" => $scope, 
            "expires" => $result['expires'],
            "clientid" => $result['client_id'],
            "type" => $result['type'] ));
    }
 

}
