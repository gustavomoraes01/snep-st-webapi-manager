<?php

/**
 * Classe managerService - Classe para gerir cadastro de clientes e, opcionalmente,
 * usuario de acesso a api, tokens e serviços
 *
 * @category  Services
 * @package   services_managerService
 * @author Opens Tecnologia
 */
class managerService implements SnepService {


    public function __construct($data){

        $this->data = $data;

        $this->config = new Zend_Config_Ini("services/manager/confs/setup.conf");

        // API's List
        $classes = glob("services/manager/lib/*.php");
        $this->apis = array();
        foreach($classes as $key => $file){
            $api = basename($file,".php");
            array_push($this->apis,$api);
            require( $file );
        }

        // Apis Actions list and access permissions
        // Global - access from Global Token
        // client - access from Client Token
        // ===>>> a Fazer :  Implementar em Banco de Dados - 01/03/2016 <<<===
        $this->apis_actions = array(
            "client" => array(
                "get" => array("global"),
                "add" => array("global"),
                "update" => array("global","client"),
                "delete" => array("global","client"),
                "addComplete"=> array("global"),
                "addPaymentControl"=> array("global"),
                "getPaymentControl"=> array("global"),
                "updatePaymentControl"=> array("global")
                ),
            "apiUser" => array(
                "add" => array("global","client"),
                "update" => array("global","client"),
                "delete" => array("global","client")
                ),
            "tokenUser" => array(
                "add" => array("global","client"),
                "delete" => array("global","client"),
                "update" => array("global")
                ),
            "serviceUser" => array(
                "add" => array("global","client"),
                "update" => array("global","client"),
                "delete" => array("global","client")
                ),
             "credits" => array(
                "update" => array("global","client"),
                "add" => array("global","client"),
                ),
             "validate" => array(
                "get" => array("global","client"),
                ),
            );

        // Global token - Default:  user_id=opens / client_id = 1000
        $mdb = Zend_Registry::get('mdb');

        $select = $mdb->select()
                ->from('oauth_access_tokens')
                ->where('client_id = "1000" ')
                ->where('user_id = "opens"')
                ->where('type = "permanent"')
                ->where('expires > now()') ;

        $stmt = $mdb->query($select);
        $result = $stmt->fetchAll();

        //$this->global_token = $result[0]['access_token'] ;
        $this->global_token = "9edff65afa622d80dbe87e00215f3691" ;
    }

    /**
     * execute - Executa as ações do serviço
     * @return <array>
     */
    public function execute() {

        // Verificando parametros
	    if(!isset($this->data->api)) {
			api_error("You need inform the parameter: api");
		}

        // Verify if API exist
        if (array_search($this->data->api, $this->apis) === false) {
            api_error("The parameter 'api' is incorrect. Verify.");
        }

        // Verify if API action is valid
        if (array_key_exists($this->data->action, $this->apis_actions[$this->data->api]) === false) {
            api_error("The parameter action for this api is incorrect. Verify.");
        }

        // ----- Start ------ Verify authorization for token/client_id
        $token = $this->data->token ;
        $funcao = $action = $this->data->action ;
        $client_id = $this->data->parameters[0]->client_id ;
        $api =  $this->data->api ;
        $access_status = false ;

        // 1 - Verify acces tokens
        if ($token === $this->global_token) {     // Verify Global Token
            if (is_numeric(array_search("global",$this->apis_actions[$this->data->api][$action]))) {
                $access_status = true ;
            }
        } else {
            //if (isset($client_id) && $client_id != "") {    // Verify client_id token
            if (isset($token) && $token != "") {    // Verify client_id token

               $mdb = Zend_Registry::get('mdb');

                $select = $mdb->select()
                    ->from('oauth_access_tokens')
                    //->where('client_id = ? ',$client_id)
                    ->where('access_token = ? ',$token)
                    ->where('expires > now()') ;

                $stmt = $mdb->query($select);
                $result = $stmt->fetchAll();

                $scope = $tokens = array();
                foreach ($result as $key => $value) {
                    array_push($scope, explode("," , $value['scope']) );
                    array_push($tokens, $value['access_token'] );
                }
                if($result[0]['client_id'] != ""){
                  $client_id = $result[0]['client_id'];
                  if (is_numeric(array_search($token, $tokens)) && is_numeric(array_search("client",$this->apis_actions[$this->data->api][$action]))) {
                    $access_status = true ;
                  }
                }
            }
        }


        if (!$access_status) {
            api_error("Invalid global token or client token. Verify documentation.");
        }

        switch ($this->data->api) {
            case 'client':
                if($funcao == 'get' || $funcao == 'getPaymentControl' || $funcao == 'updatePaymentControl'){
		            client::$funcao($this->data,$funcao);
                }elseif($funcao == 'addPaymentControl' ){
                    client::$funcao($this->data->parameters[0]) ;
		        }else{
                    if (client::verifyParameters($this->data->parameters[0],$action) ) {
                        client::$funcao($this->data->parameters[0]) ;
                    } else {
                        api_error("client: An unexpected error has occurred. Contact system administrator.");
                    }
                }
                break;

            case 'apiUser':
                if (apiUser::verifyParameters($this->data->parameters[0],$action)){
                    apiUser::$funcao($this->data->parameters[0],$funcao) ;
                } else {
                    api_error("apiUser: An unexpected error has occurred. Contact system administrator.");
                }
                break;

            case 'tokenUser':
                if($funcao == 'update'){
                    tokenUser::$funcao($this->data->parameters[0],$funcao);
                }else{
                    if (tokenUser::verifyParameters($this->data->parameters[0],$action)){
                    tokenUser::$funcao($this->data->parameters[0],$funcao) ;
                    } else {
                        api_error("tokenUser: An unexpected error has occurred. Contact system administrator.");
                    }
                }
                break;

            case 'serviceUser':

                $services = $this->data->parameters[0];
                if (serviceUser::verifyParameters($services,$action)){
                    serviceUser::$funcao($services,$funcao) ;
                } else {
                    api_error("serviceUser: An unexpected error has occurred. Contact system administrator.");
                }
                break;

            case 'credits':

                $data = $this->data->parameters[0];
                if (credits::verifyParameters($data,$action)){
                    credits::$funcao($data,$funcao) ;
                } else {
                    api_error("credits: An unexpected error has occurred. Contact system administrator.");
                }
                break;

            case 'validate':
            
                if (validate::verifyParameters($this->data,$action)){
                    validate::$funcao($this->data,$funcao) ;
                } else {
                    api_error("validate: An unexpected error has occurred. Contact system administrator.");
                }
                break;
        }	
    }
}
